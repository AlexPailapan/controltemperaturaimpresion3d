# Control de Temperatura de una impresora 3D

## Control de un ventilador de capa, no invasivo, para impresora 3D FDM

Este es un proyecto que usa Arduino y Processing para controlar la temperatura de la impresión con PLA de una impresora 3D FDM que no disponga del ventilador de capa para enfriar el depósito de PLA caliente (modelo de referencia sin ventilador de capa: Anet A8M).

Consiste en el control de temperatura, no invasivo para la impresora 3D, compuesto por un sensor termistor (100k) que obtiene la temperatura del extrusor y detecta el funcionamiento. Otro sensor (DHT11) obtiene la temperatura ambiental y la humedad relativa, para crear condiciones en conjunto y mediante PWM actuar un ventilador dirigido hacia el depósito de la impresión.


## Presentación del desarrollo del proyecto

 - [ ] [Video](https://alumnosuach-my.sharepoint.com/:v:/g/personal/apailapan_alumnos_uach_cl/EVcVdCrADtZHiCPDUrtYdtkB1QCiDnWCjsBjJj61KbaMMw?e=xckZGb)
