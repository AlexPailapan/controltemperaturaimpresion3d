#include <DHT.h>                // Importa la libreria DHT
#include <DHT_U.h>
int dht11_data = 2;             // DHT11(pin DATA) al pin digital 2
int dht11_temperatura;
int dht11_humedad;

float R1=100000;
float A=2.114990448e-3, B=0.3832381228e-4, C=5.228061052e-7; // Coeficientes Steinhart-Hart Termistor 100k

DHT dht(dht11_data, DHT11);     // Creación del objeto

int pwm_ventilador = 3;   // pin para controlar pwm del ventilador
int pwm=0;                //Variable para visualizar pwm en el monitor

void setup(){
  Serial.begin(9600);           // Inicializacion de monitor serial
  dht.begin();                  // Inicializacion de sensor DHT11
pinMode(pwm_ventilador, OUTPUT);          // pwm ventilador  
}

double Thermister(int RawADC) {     
double R, logR, Temp;  
R=R1*((1024.0/(float)RawADC)-1);
logR = log(R);
Temp = 1 / (A + (B + (C*logR*logR))*logR);
Temp = Temp-273.15;                 // Kelvin a grados Celsius
return Temp;
}

void loop(){
  dht11_temperatura = dht.readTemperature();  // Obtención del valor de temperatura
  dht11_humedad = dht.readHumidity();         // Obtención del valor de humedad
  //Serial.print("Temperatura_DHT11:");         // Escritura de los valores en el monitor serial 
  Serial.print(dht11_temperatura);
  Serial.print(",");
  //Serial.print("Humedad_DHT11:");
  Serial.print(dht11_humedad);
  Serial.print(",");

  int val;                          //Crea una variable entera para el termistor
  double termistor_temp;            //Variable de temperatura del termistor
  val=analogRead(0);                //Lee el valor del pin analogo 0 y lo mantiene como val
  termistor_temp=Thermister(val);   //Realiza la conversión del valor analogo a grados Celsius
  //Serial.print("Temperatura_Termistor:");
  Serial.print(termistor_temp);   //Escribe la temperatura en el monitor serial
  Serial.print(",");
  //Serial.print("PWM VENTILADOR:  ")  // escribe pwm del ventilador en el monitor
  Serial.print(pwm);
  Serial.println(",");

if (Serial.available()>0){
  byte dato = Serial.read();
  if(dato==65){ 

                            //condiciones de operacion del sistema
  if ( termistor_temp >=180 && dht11_temperatura >= 15  && 30 >= dht11_temperatura )     {
      analogWrite(pwm_ventilador,pwm=200);
    }

  else if (termistor_temp >180 && dht11_temperatura > 30  ) {
     analogWrite(pwm_ventilador,pwm=255);
    }
  
  else if (termistor_temp >180 && dht11_temperatura < 15 ) {
     analogWrite(pwm_ventilador,pwm=50);
    }
  }
} 
  
else{
analogWrite(pwm_ventilador,pwm=0);
}

delay(1000);                       //Espera y reinicia el loop
}
delay(1000);                       //Espera y reinicia el loop
}
