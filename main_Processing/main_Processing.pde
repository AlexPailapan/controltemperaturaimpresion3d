import processing.serial.*; //Importa la librería Serial

Serial port;  //Nombre del puerto serie

float dato1=0;  //Variales para los valores recibidos de Arduino
float dato2=0;
float dato3=0;
float dato4=0;

int rcuad=75;  //Radio del botón cuadrado
int xcuad=150;  //Posición X de rect
int ycuad=150;  //Posición Y de rect

boolean mouseRect = false;  //Indicará si el mouse encima del botón

String texto="Modo impresión PLA: APAGADO";//Texto del status inicial de la salida

boolean EstadoColor=false; //Estado para el color del botón

// Colores RGB
int colorR=0;
int colorG=0;
int colorB=0;

void setup(){
  println(Serial.list()); //Visualiza los puertos serie disponibles en la consola de abajo
  port = new Serial(this, Serial.list()[0], 9600); //Abre el puerto serie COM
  //port = new Serial(this,"COM7", 9600); //Abre el puerto serie COM7
  
  port.bufferUntil('\n');
  
  size(600, 300); //Crea una ventana. (píxeles de ancho, píxeles de alto)
}

void draw(){
  background(255,255,255);  //Fondo
  
  //Si el mouse está en rect, este cambia el borde de color
    if(mouseX > xcuad-rcuad && mouseX < xcuad+rcuad &&
     mouseY > ycuad-rcuad && mouseY < ycuad+rcuad)
     {
       mouseRect=true;  //Variable que demuestra que el mouse esta dentro de rectángulo (rect)
       stroke(255,0,0);  //Contorno rojo
     }
   else
   {
     mouseRect=false;  //Si el mouse no está dentro del rectángulo, mouseRect es falsa
     stroke(0,0,0);  //Contorno negro
   }
  
  //Rectangulo botón
  fill(colorR,colorG,colorB);
  rectMode(RADIUS); //El ancho del rectangulo está controlado por el "radio" (rcuad)
  rect(xcuad,ycuad,rcuad,rcuad);
  
  fill(0,0,0);
  //Texto
  textSize(12);
  text(texto, xcuad-rcuad, ycuad+rcuad+20);
  
  //Titulo
  text("DATOS Y CONTROL DEL VENTILADOR DE CAPA - IMPRESIÓN 3D PLA", 100, 10);
  
  //Visualización del valor de los sensores en texto
  text("Temperatura ambiental =",300,120);
  text(dato1, 450, 120);
  text("ºC",500,120);
  text("Humedad relativa =",300,140);
  text(dato2, 450, 140);    
  text("%",500,140);
  text("Temperatura extrusor =",300,160);
  text(dato3, 450, 160);
  text("ºC",500,160);
  text("Ventilador =",300,180);
  text(dato4, 450, 180);
  text("%",500,180); 
  
  if (EstadoColor==true){
     port.write("A");
     println(EstadoColor);
     delay(1000);
   }
   
  if (dato1>60){
  fill(255,0,0);
  textSize(17);
  text("¡ALTA TEMPERATURA AMBIENTAL!", 290, 240);
  }
  
    if (dato1<15){
  fill(255,0,0);
  textSize(17);
  text("¡BAJA TEMPERATURA AMBIENTAL!", 290, 240);
  }
  
   
  if (dato2>60){
  fill(0,0,255);
  textSize(17);
  text("¡ALTA HUMEDAD!", 350, 270);
  }
  

  
}

void serialEvent (Serial port) { //Lectura puerto serial
  String portData = port.readStringUntil('\n');
  if(portData != null){
    String[] stringData = split(portData, ',');
    float[] values = float(stringData);
    dato1=values[0];
    dato2=values[1];
    dato3=values[2];
    dato4=values[3]*100/255;
  }
}

void mousePressed(){  //Cuando el mouse está presionado
  if (mouseRect==true) //Si el mouse está dentro del rectángulo
  {
    EstadoColor=!EstadoColor; //El estado del color cambia
    if(EstadoColor==true)
    {
      colorR=0;
      colorG=255; //Si está presionado, el color es verde
      colorB=0;
      texto="Modo impresión PLA: ENCENDIDO";
    }
    if(EstadoColor==false)
    {
      colorR=0; //Cuando no está presionado, el color el negro
      colorG=0;
      colorB=0;
      texto="Modo impresión PLA: APAGADO";
    }
  }
}
